﻿
using UnityEngine;

public class CatMiau : MonoBehaviour
{
    [SerializeField]
    AudioSource miauClip;

    [SerializeField]
    Animator catAnimator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            catAnimator.SetTrigger("miau");
            miauClip.Play();
        }
    }
}
