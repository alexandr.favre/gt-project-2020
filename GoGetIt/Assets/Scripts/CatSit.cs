﻿
using UnityEngine;

public class CatSit : MonoBehaviour
{
    [SerializeField]
    Animator catAnimator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") catAnimator.SetBool("sitting", true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") catAnimator.SetBool("sitting", false);
    }
}
