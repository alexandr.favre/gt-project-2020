﻿
using Photon.Pun;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Launcher : PunBehaviour
{
    [SerializeField]
    GameObject mainCanvas;

    [SerializeField]
    GameObject mainCanvasFeedback;

    [SerializeField]
    GameObject mainCanvasPanel;

    [SerializeField]
    GameObject soloCanvas;

    [SerializeField]
    TMP_Dropdown soloCanvasDropdown;

    [SerializeField]
    GameObject multiCanvas;

    [SerializeField]
    GameObject joinCanvas;

    [SerializeField]
    InputField roomNameField;

    [SerializeField]
    GameObject joinCanvasError;

    [SerializeField]
    GameObject joinCanvasFeedback;

    [SerializeField]
    GameObject joinCanvasPanel;

    [SerializeField]
    GameObject createCanvas;

    [SerializeField]
    TMP_Dropdown createCanvasDropdown;

    [SerializeField]
    GameObject createCanvasFeedback;

    [SerializeField]
    GameObject createCanvasPanel;

    [SerializeField]
    TextAsset levelsJson;

    public void Awake()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        PhotonNetwork.AutomaticallySyncScene = true;

        // Load levels from json
        GameManager.levels = JsonUtility.FromJson<Levels>(levelsJson.text).levels;
        soloCanvasDropdown.ClearOptions();
        foreach (Level level in GameManager.levels)
        {
            soloCanvasDropdown.options.Add(new TMP_Dropdown.OptionData() { text = level.objectName });
            createCanvasDropdown.options.Add(new TMP_Dropdown.OptionData() { text = level.objectName });
        }
        GameManager.actualLevelIndex = 0;
    }

    public override void OnConnectedToMaster()
    {
        mainCanvasPanel.SetActive(true);
        mainCanvasFeedback.SetActive(false);
        mainCanvas.SetActive(false);
        multiCanvas.SetActive(true);
        base.OnConnectedToMaster();
    }

    public override void OnJoinedRoom()
    {
        StartGame();
        base.OnJoinedRoom();
    }

    public override void OnCreatedRoom()
    {
        GameManager.actualLevelIndex = createCanvasDropdown.value;
        PhotonExtensions.SetCustomProperty(PhotonNetwork.CurrentRoom, "level", GameManager.actualLevelIndex);
        base.OnCreatedRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        joinCanvasFeedback.SetActive(false);
        joinCanvasError.GetComponent<Text>().text = "There is no room available";
        joinCanvasError.SetActive(true);
        joinCanvasPanel.SetActive(true);
        base.OnJoinRoomFailed(returnCode, message);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        joinCanvasFeedback.SetActive(false);
        joinCanvasError.GetComponent<Text>().text = roomNameField.text + " is not available";
        joinCanvasError.SetActive(true);
        joinCanvasPanel.SetActive(true);
        base.OnJoinRoomFailed(returnCode, message);
    }

    public void Connect()
    {
        GameManager.isSolo = false;
        mainCanvasPanel.SetActive(false);
        mainCanvasFeedback.SetActive(true);
        PhotonNetwork.GameVersion = "0.0.0";
        PhotonNetwork.ConnectUsingSettings();
    }

    public void Disconnect()
    { 
        PhotonNetwork.Disconnect();
    }

    public void Join()
    {
        joinCanvasPanel.SetActive(false);
        joinCanvasFeedback.SetActive(true);
        string roomName = roomNameField.text;
        if (roomName == "") PhotonNetwork.JoinRandomRoom();
        else PhotonNetwork.JoinRoom(roomNameField.text);
    }

    public void Create()
    {
        createCanvasPanel.SetActive(false);
        createCanvasFeedback.SetActive(true);
        PhotonNetwork.CreateRoom(PhotonNetwork.NickName);
    }

    void StartGame()
    {
        Debug.Log("Start");
        GameManager.isSolo = false;
        PhotonNetwork.LoadLevel("Main");
    }

    public void PlaySolo()
    {
        GameManager.isSolo = true;
        GameManager.actualLevelIndex = soloCanvasDropdown.value;
        SceneManager.LoadScene("Main");
    }

    public void CloseApplication()
    {
        Application.Quit();
    }
}
