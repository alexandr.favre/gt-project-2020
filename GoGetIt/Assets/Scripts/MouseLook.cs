﻿
using Photon.Pun;
using UnityEngine;

public class MouseLook : MonoBehaviourPun
{
    [SerializeField]
    float mouseSensivity = 1000f;

    [SerializeField]
    GameObject playerHead = null;

    float xRotation = 0f;

    void Start()
    {
        playerHead.transform.GetChild(0).gameObject.SetActive(photonView.IsMine);
    }

    void Update()
    {
        if (!photonView.IsMine) return;

        float mouseX = Input.GetAxis("Mouse X") * mouseSensivity;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensivity;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        playerHead.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);
    }
}
