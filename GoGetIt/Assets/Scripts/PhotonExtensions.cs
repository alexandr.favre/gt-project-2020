﻿
using Photon.Realtime;

public static class PhotonExtensions
{
    public static void SetCustomProperty(this Room room, string propName, object value)
    {
        ExitGames.Client.Photon.Hashtable prop = new ExitGames.Client.Photon.Hashtable();
        prop.Add(propName, value);
        room.SetCustomProperties(prop);
    }
}
