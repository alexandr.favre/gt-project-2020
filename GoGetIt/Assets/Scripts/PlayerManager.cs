﻿
using System;
using cakeslice;
using Photon.Pun;
using UnityEngine;

public class PlayerManager : MonoBehaviourPun, IPunObservable
{
    [SerializeField]
    GameObject folder;

    [SerializeField]
    GameObject towel;

    [NonSerialized]
    public float finalTime = 0;

    [NonSerialized]
    public string selectedObject;

    [NonSerialized]
    public string playerName;

    GameObject lastRaycastHit;

    void Start()
    {
        playerName = GetComponent<PhotonView>().Owner.NickName;
    }

    public bool IsMine()
    {
        return photonView.IsMine;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(finalTime);
            stream.SendNext(selectedObject);
        }
        else
        {
            // Network player, receive data
            finalTime = (float)stream.ReceiveNext();
            selectedObject = (string)stream.ReceiveNext();
        }
    }

    void Update()
    {
        switch (selectedObject)
        {
            case "Folder":
                folder.SetActive(true);
                break;
            case "Towel":
                towel.SetActive(true);
                break;
        }

        if (!photonView.IsMine || !Camera.main) return;

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            GameObject hitGameObject = hit.transform.gameObject;
            if (lastRaycastHit != null && lastRaycastHit != hitGameObject)
            {
                Outline outline = lastRaycastHit.GetComponent<Outline>();
                if (outline != null)
                {
                    outline.enabled = false;
                }
            }
            if (hitGameObject.tag == "Object")
            {
                if (hitGameObject.name != lastRaycastHit.name && hitGameObject.name != selectedObject) hitGameObject.GetComponent<Outline>().enabled = true;
                if (Input.GetMouseButton(0) && selectedObject == null)
                {
                    selectedObject = hitGameObject.name;
                    Destroy(hitGameObject);
                    TextLogControl logControl = GameObject.FindGameObjectWithTag("TextLogControl").GetComponent<TextLogControl>();
                    logControl.LogTextRPC(playerName + " : Picked an object");
                }
            }
            lastRaycastHit = hitGameObject.transform.gameObject;
        }
    }
}
