﻿
using System;
using cakeslice;
using UnityEngine;

public class PlayerManagerSolo : MonoBehaviour
{
    [SerializeField]
    GameObject folder;

    [SerializeField]
    GameObject towel;

    [NonSerialized]
    public float finalTime = 0;

    [NonSerialized]
    public string selectedObject;

    GameObject lastRaycastHit;

    void Start()
    {
    }

    void Update()
    {
        switch (selectedObject)
        {
            case "Folder":
                folder.SetActive(true);
                break;
            case "Towel":
                towel.SetActive(true);
                break;
        }

        if (!Camera.main) return;

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            GameObject hitGameObject = hit.transform.gameObject;
            if (lastRaycastHit != null && lastRaycastHit != hitGameObject)
            {
                Outline outline = lastRaycastHit.GetComponent<Outline>();
                if (outline != null)
                {
                    outline.enabled = false;
                }
            }
            if (hitGameObject.tag == "Object")
            {
                if (hitGameObject.name != lastRaycastHit.name && hitGameObject.name != selectedObject) hitGameObject.GetComponent<Outline>().enabled = true;
                if (Input.GetMouseButton(0) && selectedObject == null)
                {
                    selectedObject = hitGameObject.name;
                    Destroy(hitGameObject);
                }
            }
            lastRaycastHit = hitGameObject.transform.gameObject;
        }
    }
}
