﻿
using Photon.Pun;
using UnityEngine;

public class PlayerMovement : PunBehaviour
{
    public static bool movementEnabled;

    [SerializeField]
    float speed = 8f;

    [SerializeField]
    float gravity = -9.81f;

    Rigidbody body;
    Vector3 move;

    void Start()
    {
        movementEnabled = false;
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (!photonView.IsMine || !movementEnabled) return;

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        move = transform.right * x + transform.forward * z;
    }

    void FixedUpdate()
    {
        if (!photonView.IsMine || !movementEnabled) return;

        body.MovePosition(body.position + move * speed * Time.fixedDeltaTime);
    }
}
