﻿
using System;
using Photon.Pun;
using UnityEngine;

public class Target : PunBehaviour
{
    [SerializeField]
    GameManager gameManager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player") return;
        if (GameManager.isSolo)
        {
            PlayerManagerSolo playerManager = other.gameObject.GetComponent<PlayerManagerSolo>();
            if (playerManager.selectedObject != null)
            {
                playerManager.finalTime = Time.time - GameManager.gameStartTime;
                GameManager.isPlaying = false;
                Destroy(gameObject);
            }
        } else
        {
            PlayerManager playerManager = other.gameObject.GetComponent<PlayerManager>();
            if (playerManager.IsMine() && playerManager.selectedObject != null)
            {
                playerManager.finalTime = Time.time - GameManager.gameStartTime;
                GameManager.isPlaying = false;
                Destroy(gameObject);
                TextLogControl logControl = GameObject.FindGameObjectWithTag("TextLogControl").GetComponent<TextLogControl>();
                logControl.LogTextRPC(playerManager.playerName + " : Finished the challenge in " + GetElapsedTimeString(playerManager.finalTime));
            }
        }
    }

    private string GetElapsedTimeString(float time)
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(time);
        return string.Format("{0:D2}:{1:D2}.{2:D3}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
    }
}
