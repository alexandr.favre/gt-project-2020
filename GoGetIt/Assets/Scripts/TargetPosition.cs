﻿using UnityEngine;

[System.Serializable]
public class TargetPosition
{
    public float x;
    public float y;
    public float z;

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }
}
