﻿using Photon.Pun;
using UnityEngine;

public class TextLogControl : MonoBehaviourPun
{
    [SerializeField]
    private GameObject textTemplate;

    public void LogTextRPC(string text)
    {
        photonView.RPC("LogText", RpcTarget.All, text);
    }

    [PunRPC]
    public void LogText(string text)
    {
        GameObject newText = Instantiate(textTemplate) as GameObject;
        newText.SetActive(true);

        newText.GetComponent<TextLogItem>().SetText(text, Color.black);
        newText.transform.SetParent(textTemplate.transform.parent, false);
    }

}
