﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class Wander : MonoBehaviour
{

    public float wanderRadius;
    public float wanderTimer;

    private Transform target;
    private NavMeshAgent agent;
    private float timer;
    private GameObject Player;
    public float MobDistanceRun = 4.0f;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        timer = wanderTimer;
        if (GameObject.FindGameObjectsWithTag("Player").Length > 1)
            Player = GameObject.FindGameObjectsWithTag("Player")[Random.Range(0, 2)];
        else
            Player = GameObject.FindGameObjectWithTag("Player");

    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.isPlaying)
        {
            agent.isStopped = true;
            agent.ResetPath();
            return;
        }

        if (Time.time < GameManager.timerStartTime + GameManager.timerDurationInSeconds)
        {
            agent.isStopped = true;
            agent.ResetPath();
            return;
        }

        timer += Time.deltaTime;

        if (GameObject.FindGameObjectsWithTag("Player").Length > 1)
            Player = GameObject.FindGameObjectsWithTag("Player")[Random.Range(0, 2)];
        else
            Player = GameObject.FindGameObjectWithTag("Player");

        float distance = Vector3.Distance(transform.position, Player.transform.position);
        if (distance < MobDistanceRun)
        {
            Vector3 dirToPlayer = transform.position - Player.transform.position;
            Vector3 newPos = transform.position - dirToPlayer;
            agent.SetDestination(newPos);
        } else if (timer >= wanderTimer)
        {
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            agent.SetDestination(newPos);
            timer = 0;
        }
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }
}