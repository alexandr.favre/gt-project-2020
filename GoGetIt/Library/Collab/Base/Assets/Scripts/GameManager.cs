
using System;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : PunBehaviour
{
    public static bool isSolo;

    public static bool isPlaying;

    public static GameManager Instance;

    public static float gameStartTime;

    public static Level[] levels;

    public static int actualLevelIndex;

    bool isLeaving = false;

    [SerializeField]
    string playerPrefab;

    [SerializeField]
    Transform playerSpawner;

    [SerializeField]
    Transform nurseOneSpawner;

    [SerializeField]
    Transform nurseTwoSpawner;

    [SerializeField]
    GameObject nurseOne;

    [SerializeField]
    GameObject nurseTwo;

    [SerializeField]
    public static float timerDurationInSeconds = 5.0f;

    [SerializeField]
    GameObject startCanvas;

    [SerializeField]
    Text challengeText;

    [SerializeField]
    GameObject gameCanvas;

    [SerializeField]
    Text elapsedTimeGameText;

    [SerializeField]
    GameObject endCanvas;

    [SerializeField]
    GameObject multiplayerCanvas;

    [SerializeField]
    Text elapsedTimeEndText;

    [SerializeField]
    GameObject rankingCanvas;

    [SerializeField]
    Text winnerNameText;

    [SerializeField]
    Text winnerTimeText;

    [SerializeField]
    GameObject masterPanel;

    [SerializeField]
    TMP_Dropdown levelDropdown;

    [SerializeField]
    TextLogControl textLog;

    float elapsedTime;
    public static float timerStartTime;

    void Start()
    {
        if (isSolo)
        {
            Debug.Log("SOLO");
            ReloadScene();
            Spawn();
            multiplayerCanvas.SetActive(false);
        }
        else
        {
            if (!PhotonNetwork.IsConnected)
            {
                SceneManager.LoadScene("Launcher");
                return;
            }
            
            Instance = this;
            
            PhotonNetwork.AutomaticallySyncScene = true;
            
            foreach (Level level in levels)
            {
                levelDropdown.options.Add(new TMP_Dropdown.OptionData() { text = level.objectName });
            }
            
            Spawn();
            ReloadScene();
            if (PhotonNetwork.IsMasterClient) ReloadSceneMaster();
            multiplayerCanvas.SetActive(true);
        }
    }

    void Update()
    {
        if (isLeaving) return;

        if (Time.time < timerStartTime + timerDurationInSeconds) return;
        if (gameStartTime == 0)
        {
            gameStartTime = Time.time;
            PlayerMovement.movementEnabled = true;
            PlayerMovementSolo.movementEnabled = true;
            startCanvas.SetActive(false);
        }

        gameCanvas.SetActive(isPlaying);
        endCanvas.SetActive(!isPlaying);
        if (isPlaying)
        {
            elapsedTime = Time.time - gameStartTime;
            elapsedTimeGameText.text = "Time elapsed: " + GetElapsedTimeString(elapsedTime);
        }
        else
        {
            elapsedTimeEndText.text = GetElapsedTimeString(elapsedTime);
        }

        // Check if game has ended
        float minTime = float.MaxValue;
        GameObject winner = null;
        if (isSolo)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            float playerTime = player.GetComponent<PlayerManagerSolo>().finalTime;
            if (playerTime == 0f) return;
            if (playerTime < minTime)
            {
                minTime = playerTime;
                winner = player;
            }
        } else
        {
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                float playerTime = player.GetComponent<PlayerManager>().finalTime;
                if (playerTime == 0f) return;
                if (playerTime < minTime)
                {
                    minTime = playerTime;
                    winner = player;
                }
            }
        }

        endCanvas.SetActive(false);
        if (!isSolo) winnerNameText.text = winner.GetComponent<PlayerManager>().playerName + " won!";
        else winnerNameText.text = "You finished !";
        winnerTimeText.text = GetElapsedTimeString(minTime);
        rankingCanvas.SetActive(true);
        if (PhotonNetwork.IsMasterClient) masterPanel.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ReloadSceneMaster()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            Rigidbody playerRB = player.GetComponent<Rigidbody>();
            playerRB.velocity = Vector3.zero;
            playerRB.angularVelocity = Vector3.zero;
            player.GetComponent<PlayerManager>().selectedObject = null;
            foreach (Transform child in player.transform.GetChild(0)) if (child.tag == "HoldedObject") child.gameObject.SetActive(false);
            player.GetComponent<PlayerManager>().finalTime = 0f;
            player.transform.position = playerSpawner.position;
            player.transform.rotation = playerSpawner.rotation;
        }
        RespawnNurses();
    }

    public void ReloadScene()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        if (!PhotonNetwork.IsMasterClient)
        {
            actualLevelIndex = (int)PhotonNetwork.CurrentRoom.CustomProperties["level"];
        }
        levelDropdown.value = actualLevelIndex;

        startCanvas.SetActive(true);
        gameCanvas.SetActive(false);
        endCanvas.SetActive(false);
        rankingCanvas.SetActive(false);

        RespawnNurses();

        PlayerMovement.movementEnabled = false;
        PlayerMovementSolo.movementEnabled = false;
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Object")) Destroy(obj);
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Target")) Destroy(obj);

        Level level = levels[actualLevelIndex];
        GameObject folder = (GameObject)Instantiate(Resources.Load(level.objectName));
        folder.name = level.objectName;
        GameObject target = (GameObject)Instantiate(Resources.Load("Target"), level.targetPosition.ToVector3(), Quaternion.identity);
        target.name = "Target";
        challengeText.text = level.description;

        gameStartTime = 0;
        isPlaying = true;
        timerStartTime = Time.time;
    }

    void Spawn()
    {
        if(isSolo)
        {
            GameObject player = (GameObject)Instantiate(Resources.Load("PlayerSolo"), playerSpawner.position, playerSpawner.rotation);
        } else
        {
            GameObject player = PhotonNetwork.Instantiate(playerPrefab, playerSpawner.position, playerSpawner.rotation);
        }
    }

    void RespawnNurses()
    {
        Rigidbody n1RB = nurseOne.GetComponent<Rigidbody>();
        n1RB.velocity = Vector3.zero;
        n1RB.angularVelocity = Vector3.zero;
        Rigidbody n2RB = nurseTwo.GetComponent<Rigidbody>();
        n2RB.velocity = Vector3.zero;
        n2RB.angularVelocity = Vector3.zero;
        nurseOne.transform.position = nurseOneSpawner.position;
        nurseOne.transform.rotation = nurseOneSpawner.rotation;
        nurseTwo.transform.position = nurseTwoSpawner.position;
        nurseTwo.transform.rotation = nurseTwoSpawner.rotation;
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        ReloadScene();
        if (PhotonNetwork.IsMasterClient) ReloadSceneMaster();
        textLog.LogTextRPC(other.NickName + ": Entered room");
    }

    public override void OnPlayerLeftRoom(Player other)
    {
        ReloadScene();
        if (PhotonNetwork.IsMasterClient) ReloadSceneMaster();
        textLog.LogTextRPC(other.NickName + ": Left room");
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("Launcher");
    }

    public void LeaveRoom()
    {
        isLeaving = true;
        PhotonNetwork.LeaveRoom();
    }

    public void PlayLevel()
    {
        actualLevelIndex = levelDropdown.value;
        PhotonExtensions.SetCustomProperty(PhotonNetwork.CurrentRoom, "level", actualLevelIndex);
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            player.GetComponent<PlayerManager>().haveToReload = true;
        }
    }

    public string GetElapsedTimeString(float time)
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(time);
        return string.Format("{0:D2}:{1:D2}.{2:D3}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
    }
}
