﻿
[System.Serializable]
public class Level
{
    public string objectName;
    public TargetPosition targetPosition;

    public override string ToString()
    {
        return objectName;
    }
}
