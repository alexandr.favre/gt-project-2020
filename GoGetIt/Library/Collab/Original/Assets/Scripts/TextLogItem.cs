﻿using TMPro;
using UnityEngine;

public class TextLogItem : MonoBehaviour
{
    public void SetText(string text, Color color)
    {
        GetComponent<TextMeshProUGUI>().text = text;
        GetComponent<TextMeshProUGUI>().color = color;
    }
}
